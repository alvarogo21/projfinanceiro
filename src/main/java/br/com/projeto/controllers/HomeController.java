package br.com.projeto.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import br.com.projeto.models.Usuario;


@Controller
public class HomeController
{

		
	@RequestMapping("/")
   public String index()
   {
      return "index";
   }
   
   @RequestMapping("efetuaLogin")
   public String efetuaLogin(Usuario usuario, HttpSession session) {
		  if(new JdbcUsuarioDao().existeUsuario(usuario)) {
		    session.setAttribute("usuarioLogado", usuario);
		    return "menu";
		  }
		  return "redirect:menu";
		}
}
