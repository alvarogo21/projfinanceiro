package br.com.projeto.models;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "usuario")
public class Usuario {
    
	@Id
    @GeneratedValue
    @Basic(optional = false)
    private Integer id;
	
	@Column(name = "Nome")
	@Basic(optional = false)
    private String nome;
	
	@Column(name = "Email")
    private String email;   
	
	@Column(name = "Senha")
    private int senha;

    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return nome;
	}
	public void setName(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Integer getSenha(){
		return senha;
	}	
	public void setSenha(Integer senha){
		this.senha = senha;
	}

}
