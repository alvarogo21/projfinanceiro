package br.com.projeto.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;


@Entity
@Table(name = "historico")
public class Historico {

	@Id
    @GeneratedValue
    @Basic(optional = false)
    private Integer id;
	
	@Column(name = "Descricao")
	private String descricao;
	
	@Column(name = "Valor")
	private Float valor;	
	
	@Column(name = "Data")
	@Temporal(TemporalType.TIMESTAMP)
    private Date data;
	
	public Integer GetId(){
		return id;
	}
	public void SetId(int id){
		this.id = id;
	}
	
	
	public String GetDescricao(){
		return descricao;
	}	
	public void SetDescricao(String descricao){
		this.descricao = descricao;
	}
	
	
	public Float GetValor(){
		return valor;
	}	
	public void SetValor(Float valor){
		this.valor = valor;
	}
	
	
	public Date GetData(){
		return data;
	}	
	public void SetData(Date data){
		this.data = data;
	}
	
}
