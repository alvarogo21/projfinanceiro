package br.com.projeto.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "conta")
public class Conta {
    
	@Id
    @GeneratedValue
    @Basic(optional = false)
    private Integer id;
	
	@Column(name = "Saldo")
	private Float saldo;

	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Float getSaldo() {
		return saldo;
	}
	public void setName(Float saldo) {
		this.saldo = saldo;
	}
}

